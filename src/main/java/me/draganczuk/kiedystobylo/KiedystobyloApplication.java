package me.draganczuk.kiedystobylo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class KiedystobyloApplication {
    public static void main(String[] args) {
        SpringApplication.run(KiedystobyloApplication.class, args);
    }



}

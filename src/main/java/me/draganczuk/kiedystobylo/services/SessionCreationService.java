package me.draganczuk.kiedystobylo.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import me.draganczuk.kiedystobylo.BotMain;
import me.draganczuk.kiedystobylo.entities.CreationStep;
import me.draganczuk.kiedystobylo.entities.WIPSession;
import me.draganczuk.kiedystobylo.repos.ChannelRepository;
import me.draganczuk.kiedystobylo.repos.SessionRepository;
import me.draganczuk.kiedystobylo.repos.WIPSessionRepository;
import me.draganczuk.kiedystobylo.scheduling.SchedulerService;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.entities.Channel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.interactions.components.selections.SelectMenu;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
@Log
// TODO: Ogarnąć ten śmietnik
public class SessionCreationService {

    private final WIPSessionRepository wipSessionRepository;
    private final ChannelRepository channelRepository;
    private final SessionRepository sessionRepository;
    private final SchedulerService schedulerService;

    @Value("${rekrutacja}")
    String rekrutacja;

    @Transactional
    public void createSession(SlashCommandInteractionEvent event) {
        if (wipSessionRepository.existsByDmUser(event.getUser().getId())) {
            event.getHook().sendMessage("Już tworzysz jakąś sesję.").queue();
            return;
        }
        var session = WIPSession.builder().dmUser(event.getUser().getId()).step(CreationStep.TIER_STEP).build();
        showTierSelector(event.getHook(), session);
        wipSessionRepository.save(session);
    }

    public void showTierSelector(InteractionHook hook, WIPSession session) {
        var tierMenu = SelectMenu.create("menu:tier").addOption("Tier 1 (3-4)", "t1").addOption("Tier 2 (5-7)", "t2").addOption("Tier 3 (8-10)", "t3").addOption("Tier 4 (11-13)", "t4").addOption("Tier 5 (14-16)", "t5").addOption("Tier 6 (17-20)", "t6").setMaxValues(6).build();

        var id = hook.sendMessage("Wybierz który tier gra. Możesz zaznaczyć kilka. Wybranie tego samego tieru ponownie odznaczy go.")
//                .setEphemeral(true)
                .addActionRow(tierMenu).addActionRow(Button.primary("btn:cancel", "Anuluj")).complete().getId();
        session.setLastCreationMessage(id);
    }

    //    @Transactional
    public void onMenu(List<String> chosenValue, User user, InteractionHook hook) {
        var sessionOptional = wipSessionRepository.findByDmUser(user.getId());
        if (sessionOptional.isEmpty()) {
            hook.sendMessage("Nie znaleziono żadnej sesji dla użytkownika %s.".formatted(user.getName())).queue();
            return;
        }

        var session = sessionOptional.get();

        hook.getInteraction().getMessageChannel().deleteMessageById(session.getLastCreationMessage()).queue();

        switch (session.getStep()) {
            case TIER_STEP -> {
                session.setTiers(chosenValue.stream().map(this::tierNameToNum).toList());
                session.setStep(CreationStep.CHANNEL_STEP);
                wipSessionRepository.save(session);

                showChannelSelection(hook, session);
            }
            case CHANNEL_STEP -> {
                var channelId = chosenValue.get(0);
                var channelOpt = channelRepository.findByChannelId(channelId);

                if (channelOpt.isEmpty()) {
                    hook.sendMessage("Nie znaleziono takiego kanału. Something's wrong").queue();
                    return;
                }

                var channel = channelOpt.get();
                session.setChannel(channel);
                session.setStep(CreationStep.TIME_STEP);

                askForDate(hook, session);

                wipSessionRepository.save(session);
            }
        }
    }

    @Transactional
    void askForDate(InteractionHook hook, WIPSession session) {
        var id = hook.sendMessage("Kiedy będzie sesja? Format: [dd.mm.yyyy hh:mm], np. 24.02.2022 21:37").complete().getId();
        session.setLastCreationMessage(id);
        wipSessionRepository.save(session);
    }

    public void onUserReply(MessageReceivedEvent event) {
        var sessionOptional = wipSessionRepository.findByDmUser(event.getAuthor().getId());
        if (sessionOptional.isEmpty()) {
            return;
        }

        var session = sessionOptional.get();

        event.getChannel().deleteMessageById(session.getLastCreationMessage()).queue();

        switch (session.getStep()) {
            case TIME_STEP -> {
                timeResponse(event, session);
            }
            case NAME_STEP -> {
                nameResponse(event, session);
            }
            case DESCRIPTION_STEP -> {
                descriptionResponse(event, session);
            }
            case QUESTION_STEP -> {
                questionResponse(event, session);
            }
            case EXTRA_INFO_STEP -> {
                extraInfoResponse(event, session);
            }
        }
    }

    private void extraInfoResponse(MessageReceivedEvent event, WIPSession session) {
        var msg = event.getMessage().getContentStripped();

        event.getMessage().delete().queue();


        if (!msg.equals(".")) {
            session.setRecruit(msg);
        }

        var id = sendFinalStep(event, session);

        session.setLastCreationMessage(id);

        session.setStep(CreationStep.FINAL_STEP);

        wipSessionRepository.save(session);
    }

    @Transactional
    public String sendFinalStep(MessageReceivedEvent event, WIPSession session) {

        var builder = new EmbedBuilder()
                .setTitle("Podgląd twojej sesji")
                .setDescription("Jeśli wszystko się zgadza, kliknij \"Opublikuj\", dzięki czemu wysłane zostanie ogłoszenie. W przeciwnym przypadku, kliknij \"Anuluj\" i zacznij od nowa")
                .addField("Nazwa", session.getName(), false)
                .addField("Kiedy", "<t:%s:F>".formatted(session.getTimestamp().toEpochSecond(ZoneOffset.of("+1"))),
                        false)
                .addField("Tiery", session.getTiers().stream().map(this::tierNumToName).collect(Collectors.joining(", ")), false)
                .addField("Kanał", session.getChannel().getChannelName(), false);

        if (session.getDescription() != null)
            builder.addField("Opis", formatDescription(session.getDescription()), false);
        if (session.getRecruit() != null)
            builder.addField("Format zgłoszenia", session.getRecruit(), false);
        if (session.getExtra() != null)
            builder.addField("Dodatkowe info", session.getExtra(), false);

        MessageEmbed embed = builder.build();

        String id = event.getChannel()
                .sendMessageEmbeds(embed)
                .setActionRow(Button.primary("btn:publish", "Opublikuj"), Button.danger("btn:cancel", "Anuluj"))
                .complete()
                .getId();

        return id;
    }

    private String formatDescription(String description) {
        if(description.length() > 1000)
            return "%s...".formatted(description.substring(0, 1000));
        return description;
    }

    private void questionResponse(MessageReceivedEvent event, WIPSession session) {
        var msg = event.getMessage().getContentStripped();

        event.getMessage().delete().queue();

        var id = event.getChannel().sendMessage("Podaj dodatkowe informacje. Opcjonalny - wpisz `.` aby pominąć").complete().getId();

        session.setLastCreationMessage(id);
        if (!msg.equals("."))
            session.setRecruit(msg);
        session.setStep(CreationStep.EXTRA_INFO_STEP);

        wipSessionRepository.save(session);

    }

    private void descriptionResponse(MessageReceivedEvent event, WIPSession session) {
        var msg = event.getMessage().getContentStripped();
        event.getMessage().delete().queue();

        var id = event.getChannel().sendMessage("Podaj wzór zgłoszenia. Opcjonalny - wpisz `.` aby pominąć").complete().getId();

        session.setLastCreationMessage(id);
        if (!msg.equals("."))
            session.setDescription(msg);
        session.setStep(CreationStep.QUESTION_STEP);

        wipSessionRepository.save(session);
    }

    private void nameResponse(MessageReceivedEvent event, WIPSession session) {
        var msg = event.getMessage().getContentStripped();

        event.getMessage().delete().queue();
        var id = event.getChannel().sendMessage("Podaj opis misji. Opcjonalne - wpisz `.` aby pominąć").complete().getId();

        session.setLastCreationMessage(id);
        session.setName(msg);
        session.setStep(CreationStep.DESCRIPTION_STEP);

        wipSessionRepository.save(session);
    }

    private void timeResponse(MessageReceivedEvent event, WIPSession session) {
        var msg = event.getMessage().getContentStripped();
        LocalDateTime time = null;
        try {
            time = LocalDateTime.parse(msg, DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
        } catch (DateTimeParseException e) {
            log.info(e.getMessage());
            event.getChannel().sendMessage("Zły czas. Format: [dd.mm.yyyy hh:mm], np. 24.02.2022 21:37").queue(message -> message.delete().queueAfter(30, TimeUnit.SECONDS));
            return;
        }


        event.getChannel().deleteMessageById(session.getLastCreationMessage()).queue();
        event.getMessage().delete().queue();

        String id = event.getChannel().sendMessage("Podaj nazwę misji").complete().getId();

        session.setLastCreationMessage(id);
        session.setTimestamp(time);
        session.setStep(CreationStep.NAME_STEP);
        wipSessionRepository.save(session);

    }

    @Transactional
    void showChannelSelection(InteractionHook hook, WIPSession session) {
        var menuBuilder = SelectMenu.create("menu:channel");

        channelRepository.findAllByFreeTrue().forEach(channel -> menuBuilder.addOption(channel.getChannelName(), channel.getChannelId()));

        var menu = menuBuilder.build();

        log.info("Sending channel menu. Entries: %d".formatted(menu.getOptions().size()));
        var id = hook.sendMessage("Wybierz kanał")
//                .setEphemeral(true)
                .addActionRow(menu).addActionRow(Button.primary("btn:cancel", "Anuluj")).complete().getId();
        session.setLastCreationMessage(id);
        wipSessionRepository.save(session);
    }

    public void publish(ButtonInteractionEvent event) {
        event.deferReply().queue();
        TextChannel channel = event.getJDA().getTextChannelById(rekrutacja);

        var sessionOptional = wipSessionRepository.findByDmUser(event.getUser().getId());
        if (sessionOptional.isEmpty()) {
            event.getHook().sendMessage("Nie znaleziono żadnej sesji dla użytkownika %s.".formatted(event.getUser().getName())).queue();
            return;
        }

        var session = sessionOptional.get();

        event.getChannel().deleteMessageById(session.getLastCreationMessage()).queue();

        var chan = session.getChannel();
        chan.setFree(false);
        channelRepository.save(chan);

        var finishedSession = sessionRepository.save(session.toSession());
        schedulerService.scheduleSession(finishedSession);

        channel.sendMessage(SessionFormatter.formatMessage(finishedSession))
                .allowedMentions(List.of(Message.MentionType.CHANNEL, Message.MentionType.USER, Message.MentionType.ROLE))
                .queue();

        event.getHook().deleteOriginal().queue();


        event.getHook().sendMessage("Ogłoszenie opublikowane").queue(
                message -> message.delete().queueAfter(30, TimeUnit.SECONDS)
        );

        wipSessionRepository.delete(session);

    }

    @Transactional
    public void cancel(ButtonInteractionEvent event) {
        event.deferReply().queue();

        var user = event.getUser().getId();
        Optional<WIPSession> sessionOpt = wipSessionRepository.findByDmUser(user);
        sessionOpt.ifPresent(session -> event.getChannel().deleteMessageById(session.getLastCreationMessage()).queue());

        wipSessionRepository.deleteAllByDmUser(user);

        event.getHook().sendMessage("Tworzenie anulowano.")
                .queue(msg -> msg.delete().queueAfter(30, TimeUnit.SECONDS));
    }

    private String tierNumToPing(Integer num) {
        return Optional.ofNullable(BotMain.jda.getRoleById(tierNumToId(num))).map(IMentionable::getAsMention).orElse(tierNumToName(num));
    }

    // TODO: T1 is test
    private String tierNumToId(int num){
        return switch (num){
            case 1 -> "791013628043329558";
            case 2 -> "825364049893916683";
            case 3 -> "825364190738382888";
            case 4 -> "827114563257630750";
            case 5 -> "860812627671121951";
            case 6 -> "860812638786551818";
            default -> throw new IllegalStateException("Unexpected value: " + num);
        };
    }

    private int tierNameToNum(String tierName) {
        return switch (tierName) {
            case "t1" -> 1;
            case "t2" -> 2;
            case "t3" -> 3;
            case "t4" -> 4;
            case "t5" -> 5;
            case "t6" -> 6;
            default -> -1;
        };
    }

    private String tierNumToName(Integer num) {
        return switch (num) {
            case 1 -> "Level 3-4";
            case 2 -> "Level 5-7";
            case 3 -> "Level 8-10";
            case 4 -> "Level 11-13";
            case 5 -> "Level 14-16";
            case 6 -> "Level 17-20";
            default -> "Something fucked up";
        };
    }
}

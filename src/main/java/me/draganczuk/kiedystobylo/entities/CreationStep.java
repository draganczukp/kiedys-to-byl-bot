package me.draganczuk.kiedystobylo.entities;

public enum CreationStep {
    TIER_STEP, TIME_STEP, CHANNEL_STEP, LINK_STEP, NAME_STEP, DESCRIPTION_STEP, QUESTION_STEP, EXTRA_INFO_STEP, FINAL_STEP;
}

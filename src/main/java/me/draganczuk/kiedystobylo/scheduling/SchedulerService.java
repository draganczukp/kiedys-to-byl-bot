package me.draganczuk.kiedystobylo.scheduling;

import lombok.RequiredArgsConstructor;
import me.draganczuk.kiedystobylo.entities.Channel;
import me.draganczuk.kiedystobylo.repos.ChannelRepository;
import me.draganczuk.kiedystobylo.entities.Session;
import org.quartz.*;
import org.springframework.data.convert.Jsr310Converters;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.quartz.TriggerBuilder.newTrigger;

@Service
@RequiredArgsConstructor
public class SchedulerService {

	private final Scheduler scheduler;
	private final ChannelRepository channelRepository;

	public void scheduleSession(Session finishedSession) {
		try {
			if (!scheduler.isStarted()) {
				scheduler.start();
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		var job =
				JobBuilder.newJob(ChannelFreeingJob.class).storeDurably().withIdentity(finishedSession.getDmUser() +
						"_" + finishedSession.getName() + "_" + finishedSession.getTimestamp().toString()).usingJobData("channel_id", finishedSession.getChannel().getId()).build();
		var trigger =
				newTrigger().withIdentity(finishedSession.getDmUser() + "_" + finishedSession.getName() + "_" + finishedSession.getTimestamp().toString()).forJob(job).usingJobData("channel_id", finishedSession.getChannel().getId())
						.startAt(new Date(finishedSession.getTimestamp().toInstant(ZoneOffset.of("+1")).plus(6,
								ChronoUnit.HOURS).toEpochMilli())).build();
		try {
			scheduler.addJob(job, true, false);
			scheduler.scheduleJob(trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}

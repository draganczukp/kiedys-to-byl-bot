package me.draganczuk.kiedystobylo.repos;

import me.draganczuk.kiedystobylo.entities.WIPSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WIPSessionRepository extends JpaRepository<WIPSession, Long> {

    Optional<WIPSession> findByDmUser(String userId);

    void deleteAllByDmUser(String userId);

    boolean existsByDmUser(String userId);
}

package me.draganczuk.kiedystobylo.repos;

import me.draganczuk.kiedystobylo.entities.Channel;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface ChannelRepository extends JpaRepository<Channel, Long> {
    Optional<Channel> findByChannelId(String channelId);

    List<Channel> findAllByFreeTrue();
}
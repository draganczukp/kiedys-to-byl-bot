package me.draganczuk.kiedystobylo;

import lombok.RequiredArgsConstructor;
import me.draganczuk.kiedystobylo.services.SessionCreationService;
import me.draganczuk.kiedystobylo.services.SessionService;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.SelectMenuInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class KiedysToBylListener extends ListenerAdapter {

    private final SessionCreationService sessionCreationService;
    private final SessionService sessionService;

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        String eventName = event.getInteraction().getName();
        event.deferReply().queue();
        switch (eventName) {
            case "ping" -> {
                event.getHook().sendMessage("pong!").queue( message -> message.delete().queueAfter(30, TimeUnit.SECONDS));
            }
            case "sesja" -> {
                sessionCreationService.createSession(event);
            }
            case "lista" -> {
                sessionService.showAllSessions(event);
            }
            case "kanaly" -> {
                sessionService.showChannels(event);
            }

            default -> event.getHook().setEphemeral(true).sendMessage("Unknown command: %s".formatted(eventName)).queue();
        }
        super.onSlashCommandInteraction(event);
    }

    @Override
    public void onButtonInteraction(@NotNull ButtonInteractionEvent event) {
        if(Objects.equals(event.getButton().getId(), "btn:cancel")){
            sessionCreationService.cancel(event);
        }

        if(Objects.equals(event.getButton().getId(), "btn:publish")){
            sessionCreationService.publish(event);
        }

    }

    @Override
    public void onSelectMenuInteraction(@NotNull SelectMenuInteractionEvent event) {
        event.deferReply().queue();
        sessionCreationService.onMenu(event.getValues(), event.getUser(), event.getHook());
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        if(event.getAuthor().isBot()){
            return;
        }
        sessionCreationService.onUserReply(event);
    }
}

package me.draganczuk.kiedystobylo.services;

import me.draganczuk.kiedystobylo.entities.Session;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.apache.logging.log4j.util.Strings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

public class SessionFormatter {
	public static Message formatMessage(Session session) {
		var builder = new MessageBuilder();
		String tiers =
				session.getTiers().stream().map(SessionFormatter::tierNumToId).map("<@&%s>"::formatted).collect(Collectors.joining(", "));

		builder.append("<@%s> ogłasza sesję!\n".formatted(session.getDmUser()));
		builder.append("**Quest: **%s\n".formatted(session.getName()));
		builder.append("**Grają: **%s\n".formatted(tiers));
		builder.append("**Kiedy: **<t:%s:F>\n".formatted(session.getTimestamp().toEpochSecond(ZoneOffset.of("+1"))));
		builder.append("**Kanał: **<#%s>\n".formatted(session.getChannel().getChannelId()));

		if (Strings.isNotBlank(session.getRecruit()))
			builder.append("**Format:\n**%s\n\n".formatted(session.getRecruit()));
		else builder.append("**Format:\n**Imię/Rasa/Klasa i poziom\n");

		if (Strings.isNotBlank(session.getDescription()))
			builder.append("**Opis:**\n%s\n\n".formatted(session.getDescription()));

		if (Strings.isNotBlank(session.getExtra())) builder.append("**Dodatkowe:**\n%s".formatted(session.getExtra()));

		builder.allowMentions(Message.MentionType.CHANNEL, Message.MentionType.ROLE, Message.MentionType.USER);

		return builder.build();
	}

	private static String tierNumToId(int num) {
		return switch (num) {
			case 1 -> "825363814437617754";
			case 2 -> "825364049893916683";
			case 3 -> "825364190738382888";
			case 4 -> "827114563257630750";
			case 5 -> "860812627671121951";
			case 6 -> "860812638786551818";
			default -> throw new IllegalStateException("Unexpected value: " + num);
		};
	}

}

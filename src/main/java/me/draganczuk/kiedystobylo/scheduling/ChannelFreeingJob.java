package me.draganczuk.kiedystobylo.scheduling;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import me.draganczuk.kiedystobylo.entities.Channel;
import me.draganczuk.kiedystobylo.repos.ChannelRepository;
import me.draganczuk.kiedystobylo.services.SessionService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

//@RequiredArgsConstructor
@NoArgsConstructor
@Log
@Component
public class ChannelFreeingJob extends QuartzJobBean {

    @Autowired
    protected ChannelRepository channelRepository;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        var channelId = ((Long) context.getJobDetail().getJobDataMap().get("channel_id"));
        var channelOptional = channelRepository.findById(channelId);
        if(channelOptional.isEmpty()){
            return;
        }
        var channel = channelOptional.get();
        log.info("Freeing channel %s".formatted(channel.getChannelName()));
        channel.setFree(true);
        channelRepository.save(channel);
    }
}

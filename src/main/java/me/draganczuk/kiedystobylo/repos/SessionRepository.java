package me.draganczuk.kiedystobylo.repos;

import me.draganczuk.kiedystobylo.entities.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface SessionRepository extends JpaRepository<Session, Long> {

    List<Session> findAllByTimestampAfter(LocalDateTime now);

}
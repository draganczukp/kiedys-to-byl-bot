package me.draganczuk.kiedystobylo.entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WIPSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private Channel channel;

    private LocalDateTime timestamp;
    private String dmUser;

    @ElementCollection(fetch = FetchType.EAGER)
    @Builder.Default
    private List<Integer> tiers = new ArrayList<>();

    private String name;
    private String roll;
    private String recruit;

    @Lob
    @Column(name = "description", columnDefinition = "LONGTEXT")
    private String description;
    private String extra;

    private String lastCreationMessage;

    @Enumerated
    private CreationStep step;

    public Session toSession(){
        return Session.builder()
                .dmUser(dmUser)
                .channel(channel)
                .description(description)
                .extra(extra)
                .name(name)
                .recruit(recruit)
                .tiers(tiers)
                .timestamp(timestamp)
                .build();
    }
}

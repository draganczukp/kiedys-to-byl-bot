package me.draganczuk.kiedystobylo.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import me.draganczuk.kiedystobylo.entities.Channel;
import me.draganczuk.kiedystobylo.entities.Session;
import me.draganczuk.kiedystobylo.repos.ChannelRepository;
import me.draganczuk.kiedystobylo.repos.SessionRepository;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
@RequiredArgsConstructor
@CommonsLog
public class SessionService {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM HH:mm");

    private final SessionRepository sessionRepository;
    private final ChannelRepository channelRepository;

    public void showAllSessions(SlashCommandInteractionEvent event) {
        var builder = new MessageBuilder("Nadchodzące sesje:\n");
        sessionRepository.findAllByTimestampAfter(LocalDateTime.now())
                .stream()
                .map(this::sessionToString)
                .forEach(builder::append);

        builder.allowMentions(Message.MentionType.CHANNEL);

        event.getHook().sendMessage(builder.build())
                .queue();
    }

    private String sessionToString(Session session){
        log.info("Session: %s, time: %s, timestamp: %d".formatted(session.getName(),
                session.getTimestamp().toString(), session.getTimestamp().toEpochSecond(ZoneOffset.of("+1"))));
        return "<t:%d:F> | <#%s> | %s\n"
                .formatted(
                        session.getTimestamp().toEpochSecond(ZoneOffset.of("+1")),
                        session.getChannel().getChannelId(),
                        session.getName()
                );
    }

    public void showChannels(SlashCommandInteractionEvent event) {

//        event.deferReply().queue();

        var builder = new MessageBuilder("🟢 - wolne, 🔴 - zajęte");

        channelRepository.findAll()
                .stream()
                .map(this::channelToString)
                .forEachOrdered(builder::append);

        event.getHook().sendMessage(builder.build()).queue();

    }

    private String channelToString(Channel channel) {
        return "%s <#%s>\n".formatted(
                channel.isFree() ? "🟢" : "🔴",
                channel.getChannelId()
        );
    }
}

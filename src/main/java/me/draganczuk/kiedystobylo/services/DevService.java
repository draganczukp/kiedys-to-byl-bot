package me.draganczuk.kiedystobylo.services;

import lombok.RequiredArgsConstructor;
import me.draganczuk.kiedystobylo.repos.ChannelRepository;
import me.draganczuk.kiedystobylo.entities.Channel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DevService {
	private final ChannelRepository channelRepository;

	@Value("${dev}")
	boolean dev;

	public void populateDB() {
		if (channelRepository.count() == 0) {
			if (dev) {
				channelRepository.saveAll(List.of(Channel.builder().channelId("944594206033334352").free(true).channelName("test-1").build(), Channel.builder().channelId("944594218578477106").channelName("test-2").free(false).build()));
			} else {
				channelRepository.saveAll(List.of(Channel.builder().channelId("757996970596892752").channelName("Kiedy"
						+ "ś to było granie").build(), Channel.builder().channelId("757997131809161326").channelName(
								"Smoki i lochy").build(),
						Channel.builder().channelId("757997462702260254").channelName("Oneshot funshot").build(),
						Channel.builder().channelId("757997639370277195").channelName("Murder hobos").build(),
						Channel.builder().channelId("886588703089262655").channelName("Rules lawyers").build(),
						Channel.builder().channelId("916823051834896474").channelName("Role-play gaming").build(),
						Channel.builder().channelId("886272012350287913").channelName("No TPK gang").build(),
						Channel.builder().channelId("770366784951681094").channelName("Multiszoty").build(),
						Channel.builder().channelId("941119679949316207").channelName("GL fighting Dragons").build()));
			}
		}
	}
}

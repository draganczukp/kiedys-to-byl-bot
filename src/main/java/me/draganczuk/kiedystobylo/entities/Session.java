package me.draganczuk.kiedystobylo.entities;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private Channel channel;

    private LocalDateTime timestamp;
    private String dmUser;

    @ElementCollection(fetch = FetchType.EAGER)
    @Builder.Default
    private List<Integer> tiers = new ArrayList<>();

    private String name;
    private String roll;
    private String recruit;

    @Lob
    @Column(name = "description", columnDefinition = "LONGTEXT")
    private String description;
    private String extra;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Session sesja = (Session) o;
        return id != null && Objects.equals(id, sesja.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

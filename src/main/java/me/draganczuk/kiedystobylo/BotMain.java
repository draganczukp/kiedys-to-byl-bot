package me.draganczuk.kiedystobylo;


import lombok.RequiredArgsConstructor;
import me.draganczuk.kiedystobylo.services.DevService;
import me.draganczuk.kiedystobylo.services.SessionCreationService;
import me.draganczuk.kiedystobylo.services.SessionService;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BotMain implements CommandLineRunner {
    public static JDA jda;

    @Value("${token}")
    private String token;

    private final SessionCreationService sessionCreationService;
    private final SessionService sessionService;

    private final DevService devService;

    @Override
    public void run(String... args) throws Exception {
        devService.populateDB();
        jda = JDABuilder.createLight(token)
                .addEventListeners(new KiedysToBylListener(sessionCreationService, sessionService))
                .setStatus(OnlineStatus.ONLINE)
                .setActivity(Activity.playing("/sesja"))
                .build();
        jda.awaitReady();

        var cmds = jda.updateCommands();

        cmds.addCommands(
                Commands.slash("ping", "Ping!"),
                Commands.slash("sesja", "Stwórz nową sesję"),
                Commands.slash("lista", "Lista nadchodzących sesji"),
                Commands.slash("kanaly", "Lista kanałów i czy są w użyciu")
            ).queue();


    }
}
